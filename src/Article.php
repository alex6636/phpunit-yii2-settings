<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\simple;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string $text
 */

class Article extends ActiveRecord
{
    public function rules() {
        return [
            [['title', 'text'], 'filter', 'filter'=>'trim'],
            [['title'], 'required'],
            [['text'], 'string'],
        ];
    }
}
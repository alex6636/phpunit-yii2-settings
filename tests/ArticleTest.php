<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\simple\tests;
use alexs\yii2phpunittestcase\DatabaseTableTestCase;
use alexs\simple\Article;

class ArticleTest extends DatabaseTableTestCase
{
    protected function getTableName() {
        return 'article';
    }

    protected function getTableColumns() {
        return [
            'id'   =>'pk',
            'title'=>'string NOT NULL',
            'text' =>'string NOT NULL',
        ];
    }

    public function testCreateArticle() {
        $Article = new Article;
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
        ]);
        $Article->save();
        $this->assertInstanceOf('\alexs\simple\Article', Article::findOne(1));
    }
}
